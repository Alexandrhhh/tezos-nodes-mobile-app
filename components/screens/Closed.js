import React, { Component } from 'react';
import { View, SafeAreaView, StyleSheet, Image, Dimensions, ScrollView, TextInput, StatusBar, TouchableOpacity, Platform, Linking} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { Container, Header, Content, Footer, FooterTab, Button, Text, Spinner} from 'native-base';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import Head from '../Header';

export default class Closedscreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
          sleep_bakers: [],
          loading: false,
        }
        this.navigation = props.navigation;
        this.route = props.route;
    }

    componentDidMount() {
       const url = "https://api.tezos-nodes.com/v1/sleep_bakers";
       fetch(url)
       .then(response => response.json())
       .then(json => this.setState({ sleep_bakers: json }))
       this.loading = true;
   }

    render() {
        let { sleep_bakers } = this.state;
        if(this.loading) {
            return (
                <Container>
                <Head navigation={this.navigation} route={this.route} />
                    <ScrollView style={{backgroundColor: '#1E2233'}}>
                        <Content style={{padding: 10, paddingTop: 0}}>
                            <View style={styles.header}>
                                <Text style={styles.h2}>
                                    Closed services
                                </Text>
                            </View>
                            <View>
                                <ScrollView>
                                    <View style={{flex: 1, flexDirection: 'row', flexWrap: 'nowrap', borderRadius: 20, overflow: 'hidden'}}>
                                        <View style={{maxWidth: '55%', overflow: 'hidden'}}>
                                            <View style={styles.theadtr}>
                                                <Text style={[styles.th, {width: '100%'}]}># Baker</Text>
                                            </View>
                                            {sleep_bakers.map((baker) => (
                                                <View style={styles.tbodytr} key={baker.rank}>
                                                    <View style={[styles.td, {width: '100%', flex: 1, flexDirection: 'row', alignItems: 'center'}]}>
                                                        <Text style={{color: '#EAECF2', fontFamily: 'Circe', fontSize: 16, marginRight: 10}}>{baker.rank + 1}</Text>
                                                        <View style={{width: 24, marginRight: 5}}><Image source={{uri: baker.logo}} style={{resizeMode:'contain',width:null,height:null, flex:1}}/></View>
                                                        <Text style={styles.table_text}>{baker.name}</Text>
                                                    </View>
                                                </View>
                                            ))}
                                        </View>
                                        <View style={{maxWidth: '45%', borderLeftWidth: 1, borderLeftColor: '#30333F'}}>
                                            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                                                <View>
                                                    <View style={styles.theadtr}>
                                                        <Text style={[styles.th, {width: 360}]}>Address</Text>
                                                        <Text style={[styles.th, {width: 200}]}>Reason</Text>
                                                    </View>
                                                    {sleep_bakers.map((baker, i) => (
                                                        <View style={styles.tbodytr} key={baker.rank}>
                                                            <TouchableOpacity onPress={() => Linking.openURL('https://tzstats.com/' + baker.address)} style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
                                                                <Text style={[styles.td, {width: 360}]}>{baker.address}</Text>
                                                            </TouchableOpacity>
                                                            <Text style={[styles.td, {width: 200}]}>{baker.sleep_about}</Text>
                                                       </View>
                                                    ))}
                                                </View>
                                            </ScrollView>
                                        </View>
                                    </View>
                                </ScrollView>
                            </View>
                        </Content>
                    </ScrollView>
                </Container>
            )
        } else {
            return (
                       <Container>
                              <Head navigation={this.navigation} route={this.route} />
                           <Content style={{backgroundColor: '#1E2233', color: '#EAECF2'}}>
                               <Spinner color='#319DFE' />
                           </Content>
                       </Container>
            )
        }
    }
}

const styles = {
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 30,
        marginBottom: 10
    },
    h2: {
        color: '#EAECF2',
        fontSize: 26,
        fontFamily: 'Circe_bold',
        marginRight: 11
    },
    tbodytr: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        height: 61,
        flex: 1,
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderColor: '#30333F',
        backgroundColor: '#121520'
    },
    theadtr: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        height: 61,
        flex: 1,
        justifyContent: 'center',
        flexWrap: 'nowrap'
    },
    th: {
        backgroundColor: '#0D0F17',
        paddingTop: 23,
        paddingBottom: 23,
        paddingLeft: 15,
        color: '#737B9C',
        fontFamily: 'DINNextLTPro',
        ...Platform.select({
          ios: {
            paddingTop: 24,
          },
        }),
    },
    td: {
        paddingLeft: 15,
        paddingRight: 0,
        color: '#EAECF2',
        fontFamily: 'Circe',
        fontSize: 16,
        flex: 1,
        flexDirection: 'row'
    },
    table_text: {
        color: '#EAECF2',
        fontFamily: 'Circe',
        fontSize: 16,
        flex: 1,
        flexWrap: 'wrap'
    },
    tint: {
        fontFamily: 'Circe',
        fontSize: 16,
        paddingTop: 4,
        paddingBottom: 2,
        paddingLeft: 8,
        paddingRight: 8,
        borderWidth: 1,
        borderRadius: 5,
        flex: 0,
    },
    green: {
        color: '#30DD36',
        borderColor: '#30DD36'
    },
    red: {
        color: '#F91525',
        borderColor: '#F91525'
    },
    yellow: {
        color: '#FFAD15',
        borderColor: '#FFAD15'
    },
}