//Homescreen.js

import React, { Component } from 'react';
import { View, SafeAreaView, StyleSheet, Image, Dimensions, ScrollView, TextInput, StatusBar, TouchableOpacity, Linking} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { Container, Header,Left, Body, Right, Title, Content, Footer, FooterTab, Button, Text, Spinner} from 'native-base';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import Head from '../../Header';
import { ScrollView as GestureHandlerScrollView } from 'react-native-gesture-handler';
import SvgUri from 'expo-svg-uri';

export default class Supportscreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
        this.navigation = props.navigation;
        this.route = props.route;
    }

  render() {
      const svgUrl = '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M12 23C18.0751 23 23 18.0751 23 12C23 5.92487 18.0751 1 12 1C5.92487 1 1 5.92487 1 12C1 18.0751 5.92487 23 12 23Z" stroke="#319DFE" stroke-width="1.1" stroke-miterlimit="10"/><path d="M12 1V23" stroke="#319DFE" stroke-width="1.1" stroke-miterlimit="10"/><path d="M17.4955 12.0656C17.4955 18.1799 15.0386 23 12.0002 23C8.96183 23 6.50488 18.1799 6.50488 12.0656C6.50488 5.95141 8.96183 1 12.0002 1C15.0386 1 17.4955 5.95141 17.4955 12.0656Z" stroke="#319DFE" stroke-width="1.1" stroke-miterlimit="10"/><path d="M2.34082 6.58793L21.6588 6.44727" stroke="#319DFE" stroke-width="1.1" stroke-miterlimit="10"/><path d="M2.34082 17.4489L21.6588 17.2988" stroke="#319DFE" stroke-width="1.1" stroke-miterlimit="10"/><path d="M1 12H23" stroke="#319DFE" stroke-width="1.1" stroke-miterlimit="10"/></svg>';
      const svgTwitter = '<svg width="22" height="20" viewBox="0 0 22 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M22 3.04897L20.0147 2.78746L20.533 0.853363L18.2073 1.47658C17.4639 1.03615 16.6198 0.804688 15.7477 0.804688C13.0823 0.804688 10.9137 2.97327 10.9137 5.63867V6.26575C7.58701 6.08481 4.52464 4.50655 2.43361 1.87286L1.89297 1.19191L1.39867 1.9071C1.20296 2.19009 1.03696 2.49524 0.905029 2.81364C0.49498 3.8036 0.422302 4.87312 0.695053 5.90689C0.855347 6.515 1.13548 7.0882 1.51079 7.58905L0.744568 7.77301L0.894958 8.39975C1.2946 10.0638 2.51686 11.3582 4.08606 11.8765L3.3326 12.7587L3.82271 13.1773C4.56644 13.8125 5.48271 14.2059 6.44666 14.3116C5.12453 15.2821 3.51857 15.8177 1.84732 15.8177C1.47806 15.8177 1.1041 15.791 0.735672 15.7383L0 15.6331V16.7186L0.276779 16.9109C2.44788 18.4194 4.9968 19.2168 7.6481 19.2168C11.1029 19.2168 14.3507 17.8715 16.7936 15.4287C19.2364 12.9858 20.5817 9.73799 20.5817 6.2832V5.63867C20.5817 5.41124 20.5658 5.18381 20.5342 4.95923L22 3.04897ZM19.22 4.92029C19.2681 5.15494 19.2926 5.39664 19.2926 5.63867V6.2832C19.2926 12.704 14.0689 17.9277 7.6481 17.9277C6.07672 17.9277 4.54512 17.6167 3.12447 17.0167C5.29472 16.7085 7.29277 15.6214 8.73944 13.93L9.127 13.4768L8.41986 12.77L8.04741 12.887C7.6998 12.9961 7.33842 13.0513 6.97319 13.0513C6.34343 13.0513 5.72777 12.8836 5.18814 12.5711L6.59705 10.9216L5.33585 10.8224C4.00969 10.7182 2.8779 9.89644 2.35522 8.71194L4.17921 8.2742L3.15518 7.45158C1.9581 6.48965 1.51431 4.84291 2.05461 3.41135C4.50148 6.06735 7.9138 7.57227 11.5583 7.57227H12.2028V5.63867C12.2028 3.68393 13.7931 2.09375 15.7477 2.09375C16.4621 2.09375 17.1504 2.30524 17.7385 2.70522L17.9828 2.87122L18.71 2.67635L18.3891 3.87376L19.6181 4.03557L19.1607 4.63176L19.22 4.92029Z" fill="#319DFE"/></svg>';
      const svgTelegram = '<svg width="23" height="21" viewBox="0 0 23 21" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M22.2807 1.11914L0.720703 9.54914L6.1507 12.3591L17.6607 5.73914L9.1007 13.2591V19.8791L11.8907 16.2591L18.5707 19.8791L22.2807 1.11914Z" stroke="#319DFE" stroke-width="1.1" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/></svg>';
      const svgMail = '<svg width="22" height="17" viewBox="0 0 22 17" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M20.0664 0H1.93359C0.868742 0 0 0.867797 0 1.93359V14.8242C0 15.8864 0.864875 16.7578 1.93359 16.7578H20.0664C21.1286 16.7578 22 15.8929 22 14.8242V1.93359C22 0.871406 21.1351 0 20.0664 0ZM19.7994 1.28906L11.041 10.0475L2.20683 1.28906H19.7994ZM1.28906 14.5573V2.19437L7.49714 8.34922L1.28906 14.5573ZM2.20056 15.4688L8.41255 9.25676L10.5892 11.4147C10.8412 11.6646 11.2478 11.6637 11.4987 11.4128L13.6211 9.2904L19.7994 15.4688H2.20056ZM20.7109 14.5573L14.5326 8.37891L20.7109 2.20052V14.5573Z" fill="#319DFE"/></svg>';
      return (
          <Container>
    <Head sub={true} navigation={this.navigation} route={this.route}/>
              <ScrollView style={{backgroundColor: '#1E2233'}}>
                  <Content style={{padding:10, paddingTop: 0}}>
                      <View style={styles.header}>
                          <Text style={styles.h2}>Support</Text>
                      </View>
                      <View style={styles.block_info}>
                          <TouchableOpacity style={styles.link} onPress={() => Linking.openURL('https://tezos-nodes.com')}>
                              <SvgUri svgXmlData={svgUrl} style={styles.icon} />
                              <Text style={styles.text}>www.tezos-nodes.com</Text>
                          </TouchableOpacity>
                          <TouchableOpacity style={styles.link} onPress={() => Linking.openURL('https://twitter.com/tezosnodes')}>
                              <SvgUri svgXmlData={svgTwitter} style={styles.icon} />
                              <Text style={styles.text}>@tezosnodes</Text>
                          </TouchableOpacity>
                          <TouchableOpacity style={styles.link} onPress={() => Linking.openURL('https://t.me/tezosnodes')}>
                              <SvgUri svgXmlData={svgTelegram} style={styles.icon} />
                              <Text style={styles.text}>@tezosnodes</Text>
                          </TouchableOpacity>
                          <TouchableOpacity style={styles.link} onPress={() => Linking.openURL('mailto:support@tezos-nodes.com')}>
                              <SvgUri svgXmlData={svgMail} style={styles.icon} />
                              <Text style={styles.text}>support@tezos-nodes.com</Text>
                          </TouchableOpacity>
                      </View>
                  </Content>
              </ScrollView>
          </Container>
      );
  }
}

const styles = {
    active_menu: {
        minHeight: '100%',
        maxHeight: '100%'
    },
    text: {
        fontSize: 16,
        fontFamily: 'Circe',
        color: '#EAECF2'
    },
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 30,
        marginBottom: 10
    },
    h2: {
        color: '#EAECF2',
        fontSize: 26,
        fontFamily: 'Circe_bold',
        marginRight: 11
    },
    block_info: {
        backgroundColor: "#121520",
        borderRadius: 20,
        padding: 11
    },
    link: {
        flex: 1,
        paddingHorizontal: 20,
        height: 54,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#1E2233',
        borderRadius: 12,
        margin: 5
    },
    icon: {
        marginRight: 20
    }
}