import * as React from 'react';
import { View, SafeAreaView, StyleSheet, Image, Dimensions, ScrollView, TextInput, StatusBar} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { Container, Header,Left, Body, Right, Title, Content, Footer, FooterTab, Button, Text} from 'native-base';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import SvgUri from 'expo-svg-uri';
import * as Font from 'expo-font';

export default class Trainingstackscreen extends React.Component {
    constructor(props) {
        super(props);
        console.log(props);
    }
    render() {
        const TrainingStack = createStackNavigator();
        return (
          <TrainingStack.Navigator>
            <TrainingStack.Screen name="Training" component={TrainingScreen} />
            <TrainingStack.Screen name="Details" component={DetailsScreen} />
          </TrainingStack.Navigator>
        );
    }
}