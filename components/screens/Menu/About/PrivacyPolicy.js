//Homescreen.js

import React, { Component } from 'react';
import { View, SafeAreaView, StyleSheet, Image, Dimensions, ScrollView, TextInput, StatusBar, TouchableOpacity} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { Container, Header,Left, Body, Right, Title, Content, Footer, FooterTab, Button, Text, Spinner} from 'native-base';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import Head from '../../../Header';
import { ScrollView as GestureHandlerScrollView } from 'react-native-gesture-handler';
import SvgUri from 'expo-svg-uri';

export default class PrivacyPolicyscreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
        this.navigation = props.navigation;
        this.route = props.route;
    }

  render() {
      return (
          <Container>
    <Head sub={true} navigation={this.navigation} route={this.route}/>
              <ScrollView style={{backgroundColor: '#1E2233'}}>
                  <Content style={{padding:10, paddingTop: 0}}>
                      <View style={styles.header}>
                          <Text style={styles.h2}>Privacy policy</Text>
                      </View>
                      <View style={styles.block_info}>
                          <Text style={styles.text}>Built the Tezos Nodes app as an Open Source app. This SERVICE is provided by at no cost and is intended for use as is.{"\n"}
                          This page is used to inform visitors regarding my policies with the collection, use, and disclosure of Personal Information if anyone decided to use my Service.{"\n"}
                          If you choose to use my Service, then you agree to the collection and use of information in relation to this policy. The Personal Information that I collect is used for providing and improving the Service. I will not use or share your information with anyone except as described in this Privacy Policy.{"\n"}
                          The terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, which is accessible at Tezos Nodes unless otherwise defined in this Privacy Policy.</Text>
                            <Text style={styles.h3}>Information Collection and Use</Text>
                          <Text style={styles.text}>For a better experience, while using our Service, I may require you to provide us with certain personally identifiable information, including but not limited to e-mail. The information that I request will be retained on your device and is not collected by me in any way.{"\n"}
                          The app does use third party services that may collect information used to identify you.</Text>
                            <Text style={styles.h3}>Log Data</Text>
                          <Text style={styles.text}>I want to inform you that whenever you use my Service, in a case of an error in the app I collect data and information (through third party products) on your phone called Log Data. This Log Data may include information such as your device Internet Protocol (“IP”) address, device name, operating system version, the configuration of the app when utilizing my Service, the time and date of your use of the Service, and other statistics.</Text>
                          <Text style={styles.h3}>Cookies</Text>
                          <Text style={styles.text}>
                              Cookies are files with a small amount of data that are commonly used as anonymous unique identifiers. These are sent to your browser from the websites that you visit and are stored on your device's internal memory.{"\n"}
                              This Service does not use these “cookies” explicitly. However, the app may use third party code and libraries that use “cookies” to collect information and improve their services. You have the option to either accept or refuse these cookies and know when a cookie is being sent to your device. If you choose to refuse our cookies, you may not be able to use some portions of this Service.
                          </Text>
                          <Text style={styles.h3}>Service Providers</Text>
                          <Text style={styles.text}>
                              I may employ third-party companies and individuals due to the following reasons:{"\n"}
                                <Text style={styles.bold}>•</Text> To facilitate our Service;{"\n"}
                                <Text style={styles.bold}>•</Text> To provide the Service on our behalf;{"\n"}
                                <Text style={styles.bold}>•</Text> To perform Service-related services;{"\n"}
                                <Text style={styles.bold}>•</Text> To assist us in analyzing how our Service is used.{"\n"}
                              I want to inform users of this Service that these third parties have access to your Personal Information. The reason is to perform the tasks assigned to them on our behalf. However, they are obligated not to disclose or use the information for any other purpose.
                          </Text>
                          <Text style={styles.h3}>Security</Text>
                          <Text style={styles.text}>I value your trust in providing us your Personal Information, thus we are striving to use commercially acceptable means of protecting it. But remember that no method of transmission over the internet, or method of electronic storage is 100% secure and reliable, and I cannot guarantee its absolute security.</Text>
                          <Text style={styles.h3}>Links to Other Sites</Text>
                          <Text style={styles.text}>This Service may contain links to other sites. If you click on a third-party link, you will be directed to that site. Note that these external sites are not operated by me. Therefore, I strongly advise you to review the Privacy Policy of these websites. I have no control over and assume no responsibility for the content, privacy policies, or practices of any third-party sites or services.</Text>
                          <Text style={styles.h3}>Children’s Privacy</Text>
                          <Text style={styles.text}>These Services do not address anyone under the age of 13. I do not knowingly collect personally identifiable information from children under 13. In the case I discover that a child under 13 has provided me with personal information, I immediately delete this from our servers. If you are a parent or guardian and you are aware that your child has provided us with personal information, please contact me so that I will be able to do necessary actions.</Text>
                          <Text style={styles.h3}>Changes to This Privacy Policy</Text>
                          <Text style={styles.text}>I may update our Privacy Policy from time to time. Thus, you are advised to review this page periodically for any changes. I will notify you of any changes by posting the new Privacy Policy on this page. These changes are effective immediately after they are posted on this page.</Text>
                          <Text style={styles.h3}>Contact Us</Text>
                          <Text style={[styles.text, {marginBottom: 0}]}>If you have any questions or suggestions about my Privacy Policy, do not hesitate to contact us at support@tezos-nodes.com.</Text>
                      </View>
                  </Content>
              </ScrollView>
          </Container>
      );
  }
}

const styles = {
    active_menu: {
        minHeight: '100%',
        maxHeight: '100%'
    },
    text: {
        fontSize: 22,
        fontFamily: 'Circe',
        color: '#EAECF2'
    },
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 30,
        marginBottom: 10
    },
    h2: {
        color: '#EAECF2',
        fontSize: 26,
        fontFamily: 'Circe_bold',
        marginRight: 11
    },
    block_info: {
        backgroundColor: "#121520",
        borderRadius: 20,
        padding: 20
    },
    text: {
        color: '#737B9C',
        fontSize: 18,
        fontFamily: 'Circe',
        marginBottom: 18,
        lineHeight: 22
    },
    bold: {
        fontFamily: 'Circe_bold',
        fontSize: 18,
        color: '#319DFE'
    },
    h3: {
        color: '#EAECF2',
        fontSize: 22,
        lineHeight: 26,
        fontFamily: 'Circe_bold',
        marginVertical: 10
    },
}