//Homescreen.js

import React, { Component } from 'react';
import { View, SafeAreaView, StyleSheet, Image, Dimensions, ScrollView, TextInput, StatusBar, TouchableOpacity} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { Container, Header,Left, Body, Right, Title, Content, Footer, FooterTab, Button, Text, Spinner} from 'native-base';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import Head from '../../Header';
import { ScrollView as GestureHandlerScrollView } from 'react-native-gesture-handler';
import SvgUri from 'expo-svg-uri';

export default class Aboutscreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
        this.navigation = props.navigation;
        this.route = props.route;
    }

  render() {
      const svgArrow = '<svg width="8" height="13" viewBox="0 0 8 13" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.78279 5.99274L1.72295 0.207681C1.5828 0.0737718 1.3957 0 1.1962 0C0.996706 0 0.809609 0.0737718 0.669453 0.207681L0.223188 0.633613C-0.0671998 0.911155 -0.0671998 1.36224 0.223188 1.63936L5.31178 6.4973L0.217542 11.3606C0.0773851 11.4945 0 11.6731 0 11.8634C0 12.054 0.0773851 12.2325 0.217542 12.3665L0.663807 12.7923C0.804074 12.9262 0.99106 13 1.19056 13C1.39005 13 1.57715 12.9262 1.71731 12.7923L7.78279 7.00198C7.92328 6.86764 8.00044 6.68829 8 6.49762C8.00044 6.30622 7.92328 6.12697 7.78279 5.99274Z" fill="#319DFE"/></svg>';
      const svgBack = '<svg width="15" height="12" viewBox="0 0 15 12" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0.46967 5.46967C0.176777 5.76256 0.176777 6.23744 0.46967 6.53033L5.24264 11.3033C5.53553 11.5962 6.01041 11.5962 6.3033 11.3033C6.59619 11.0104 6.59619 10.5355 6.3033 10.2426L2.06066 6L6.3033 1.75736C6.59619 1.46447 6.59619 0.989593 6.3033 0.6967C6.01041 0.403807 5.53553 0.403807 5.24264 0.6967L0.46967 5.46967ZM15 5.25L1 5.25L1 6.75L15 6.75L15 5.25Z" fill="#737B9C"/></svg>';
      return (
          <Container>
    <Head navigation={this.navigation} route={this.route}/>
              <Content style={{backgroundColor: '#121520'}}>
                  <View style={[{width: '100%', backgroundColor: '#121520', padding: 10, paddingTop: 30}, styles.active_menu]}>
                      <TouchableOpacity style={{flex: 1, flexDirection: 'row', alignItems: 'center', paddingVertical: 15}} onPress={() => this.navigation.goBack()}>
                          <SvgUri svgXmlData={svgBack} />
                          <Text style={{color: '#737B9C', fontFamily: 'DINNextLTPro', fontSize: 18, marginLeft: 6, marginTop: 6}}>Back</Text>
                      </TouchableOpacity>
                      <View style={{borderTopWidth: 1, borderBottomWidth: 1, borderColor: '#2C3140', padding: 10, marginTop: -1}}>
                          <TouchableOpacity style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', maxHeight: 59}} onPress={() => this.navigation.navigate('TezosNodes')}>
                              <Text style={styles.text}>Tezos-nodes</Text>
                              <SvgUri svgXmlData={svgArrow} />
                          </TouchableOpacity>
                      </View>
                      <View style={{borderTopWidth: 1, borderBottomWidth: 1, borderColor: '#2C3140', padding: 10, marginTop: -1}}>
                          <TouchableOpacity style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', maxHeight: 59}} onPress={() => this.navigation.navigate('PrivacyPolicy')}>
                              <Text style={styles.text}>Privacy policy</Text>
                              <SvgUri svgXmlData={svgArrow} />
                          </TouchableOpacity>
                      </View>
                      <View style={{borderTopWidth: 1, borderBottomWidth: 1, borderColor: '#2C3140', padding: 10, marginTop: -1}}>
                          <TouchableOpacity style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', maxHeight: 59}} onPress={() => this.navigation.navigate('Terms')}>
                              <Text style={styles.text}>Terms</Text>
                              <SvgUri svgXmlData={svgArrow} />
                          </TouchableOpacity>
                      </View>
                  </View>
              </Content>
          </Container>
      );
  }
}

const styles = {
    active_menu: {
        minHeight: '100%',
        maxHeight: '100%'
    },
    text: {
        fontSize: 22,
        fontFamily: 'Circe',
        color: '#EAECF2'
    }
}