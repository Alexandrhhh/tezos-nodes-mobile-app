import React, { Component } from 'react';
import { View, SafeAreaView, StyleSheet, Image, Dimensions, ScrollView, TextInput, StatusBar, TouchableOpacity, Linking, AsyncStorage} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { Container, Header,Left, Body, Right, Title, Content, Footer, FooterTab, Button, Text, Spinner} from 'native-base';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import Head from '../../Header';

export default class Personalnewsscreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            news: {},
            loading: false
        }
        this.navigation = props.navigation;
        this.route = props.route;
    }

    async updateNews() {
        await fetch('https://api.tezos-nodes.com/v1/news?id=' + this.route.params.id)
        .then(response => response.json())
        .then(json => {
            this.setState({ news: json })
            AsyncStorage.getItem('reading')
            .then(reading => {
                let arr = [];
                if(reading !== null) {
                    arr = JSON.parse(reading)
                    arr.push(json.id)
                }
                AsyncStorage.setItem('reading', JSON.stringify(arr))
            })
            this.setState({ loading: true })
        })
    }

    componentWillUnmount() {
        this._update();
    }

    componentDidMount() {
        this.updateNews();
        this._update = this.navigation.addListener('focus', () => {
          this.updateNews();
        });
    }
  render() {
      let {news} = this.state;
      if(this.state.loading) {
          return (
              <Container>
            <Head sub={true} navigation={this.navigation} route={this.route}/>
                  <ScrollView style={{backgroundColor: '#1E2233'}}>
                      <Content style={{padding: 10, paddingTop: 0}}>
                          <View style={styles.header}><Text style={styles.h2}>{news.title}</Text></View>
                          <View style={styles.block_news}>
                              <View style={styles.news_image_block}>
                                  <Image source={{uri: news.image}} style={styles.news_image}/>
                              </View>
                              <Text style={styles.news_content}>{news.long}</Text>
                              {news.link !== null && news.link !== '' ?
                                  <Button style={styles.news_button} onPress={() => Linking.openURL(news.link)}>
                                      <Text style={styles.news_button_text}>{news.text_link}</Text>
                                  </Button>
                                  :
                                  <Text></Text>
                              }
                          </View>
                      </Content>
                  </ScrollView>
              </Container>
          )
        } else {
            return (
                <Container>
                <Head sub={true} navigation={this.navigation} route={this.route}/>
                    <ScrollView style={{backgroundColor: '#1E2233'}}>
                        <Content style={{padding: 10, paddingTop: 0}}>
                            <Spinner color='#319DFE' />
                        </Content>
                    </ScrollView>
                </Container>
            );
        }
    }
}

const styles = StyleSheet.create({
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 30,
        marginBottom: 10
    },
    h2: {
        color: '#EAECF2',
        fontSize: 26,
        fontFamily: 'Circe_bold',
        marginRight: 11
    },
    block_news: {
        backgroundColor: '#121520',
        borderRadius: 20,
        padding: 16,
        marginBottom: 30
    },
    news_image_block: {
        paddingTop: '100%',
        width: '100%',
        borderRadius: 12,
        overflow: 'hidden',
        marginBottom: 20,
    },
    news_image: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        flex: 1,
        width: null,
        height: null,
        resizeMode:'cover',
    },
    news_title: {
        fontFamily: 'Circe_bold',
        fontSize: 20,
        lineHeight: 24,
        color: '#EAECF2',
        marginBottom: 20
    },
    news_content: {
        fontFamily: 'Circe',
        fontSize: 18,
        color: '#737B9C',
        lineHeight: 22,
    },
    news_button: {
        height: 60,
        borderWidth: 1,
        borderColor: '#333952',
        borderRadius: 12,
        backgroundColor: 'transparent',
        flex: 1,
        justifyContent: 'center'
    },
    news_button_text: {
        color: '#6F7693',
        fontFamily: 'Circe_bold',
        fontSize: 16
    }

});