import React, { Component } from 'react';
import { View, SafeAreaView, StyleSheet, Image, Dimensions, ScrollView, TextInput, StatusBar, TouchableOpacity, Platform} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { Container, Header, Content, Footer, FooterTab, Button, Text, Spinner, Picker} from 'native-base';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import Head from '../Header';
import SvgUri from 'expo-svg-uri';
import DropDownPicker from 'react-native-dropdown-picker';

export default class Ratingscreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
          bakers: [],
          loading: false,
          baker_update: true,
          displayBakers: [],
          search: false,
          searchText: '',
          filter: 'rank_desc',
        }
        this.navigation = props.navigation;
        this.route = props.route;
    }

    inputSearch(text) {
        var arr = Array();
        var bakers = Object.keys(this.state.bakers);
        if(text !== '') {
            bakers.map((i) => (
                this.state.bakers[i].name.toLowerCase().indexOf(text.toLowerCase()) > -1 ?
                    arr.push(i)
                : ''
            ))
            this.setState({ displayBakers: this.filterArray(arr) });
        } else {
            this.setState({ displayBakers: this.filterArray(bakers)})
        }
        this.setState({searchText: text});
    }

    bakersUpdate() {
        this.setState({ baker_update: false})
        fetch("https://api.tezos-nodes.com/v1/bakers")
        .then(response => response.json())
        .then(json => {
            this.setState({ bakers: json })
            this.setState({ baker_update: true})
        })
    }

    filter(value) {
        if(value.indexOf('_desc') > -1) {
            var arr = this.state.displayBakers.sort((a, b) => {return this.state.bakers[a][value.replace('_desc', '')] - this.state.bakers[b][value.replace('_desc', '')]});
            this.setState({displayBakers: arr})
        } else {
            var arr = this.state.displayBakers.sort((a, b) => {return this.state.bakers[b][value] - this.state.bakers[a][value]});
            this.setState({displayBakers: arr})
        }
        this.setState({filter: value})
    }
    filterArray(bakers = []) {
        var value = this.state.filter;
        if(value.indexOf('_desc') > -1) {
            var arr = bakers.sort((a, b) => {return this.state.bakers[a][value.replace('_desc', '')] - this.state.bakers[b][value.replace('_desc', '')]});
        } else {
            var arr = bakers.sort((a, b) => {return this.state.bakers[b][value] - this.state.bakers[a][value]});
        }
        return arr;
    }

    componentDidMount() {
       const url = "https://api.tezos-nodes.com/v1/bakers";
       fetch(url)
       .then(response => response.json())
       .then(json => {
           this.setState({ bakers: json })
           this.setState({ loading: true})
           this.setState({ displayBakers: Object.keys(json)})
       })
   }

    render() {
        let { bakers, displayBakers, searchText } = this.state;
        const svgUpdate = '<svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M14 6.12497V0L11.9425 2.05623C10.6773 0.787498 8.93058 0 6.99561 0C3.1257 0 0 3.13248 0 7C0 10.8675 3.1257 14 6.99561 14C10.257 14 12.9887 11.7688 13.7636 8.75001H11.9425C11.2202 10.7888 9.28518 12.25 6.99565 12.25C4.09321 12.25 1.74237 9.90062 1.74237 7C1.74237 4.09938 4.09321 1.75001 6.99565 1.75001C8.44469 1.75001 9.74488 2.35376 10.6948 3.30315L7.87118 6.12502H14V6.12497Z" fill="#737B9C"/></svg>';
        const svgAsc = '<svg width="23" height="14" viewBox="0 0 23 14" fill="none" xmlns="http://www.w3.org/2000/svg"><rect x="11" y="1" width="12" height="2" rx="1" fill="#737B9C"/><rect x="11" y="6" width="9" height="2" rx="1" fill="#737B9C"/><rect x="11" y="11" width="7" height="2" rx="1" fill="#737B9C"/><path d="M5.38223 12.8366L8.8417 9.26556C9.05277 9.04769 9.05277 8.69434 8.8417 8.47646C8.63058 8.25854 8.28836 8.25854 8.07724 8.47646L5.54055 11.0949V1.55797C5.54055 1.24984 5.29851 1 5.00001 1C4.70154 1 4.45946 1.24984 4.45946 1.55797L4.45946 11.0949L1.92277 8.47655C1.71165 8.25863 1.36943 8.25863 1.15831 8.47655C1.05284 8.58547 1 8.72831 1 8.8711C1 9.0139 1.05284 9.15669 1.15831 9.26565L4.61778 12.8366C4.82889 13.0545 5.17112 13.0545 5.38223 12.8366Z" fill="#737B9C" stroke="#737B9C" stroke-width="0.6"/></svg>';
        const svgDesc = '<svg width="23" height="14" viewBox="0 0 23 14" fill="none" xmlns="http://www.w3.org/2000/svg"><rect x="11" y="1" width="12" height="2" rx="1" fill="#737B9C"/><rect x="11" y="6" width="9" height="2" rx="1" fill="#737B9C"/><rect x="11" y="11" width="7" height="2" rx="1" fill="#737B9C"/><path d="M4.61777 1.16344L1.1583 4.73444C0.947232 4.95231 0.947232 5.30566 1.1583 5.52354C1.36942 5.74146 1.71164 5.74146 1.92276 5.52354L4.45945 2.90506V12.442C4.45945 12.7502 4.70149 13 4.99999 13C5.29846 13 5.54054 12.7502 5.54054 12.442L5.54054 2.90506L8.07723 5.52345C8.28835 5.74137 8.63057 5.74137 8.84169 5.52345C8.94716 5.41453 9 5.27169 9 5.1289C9 4.9861 8.94716 4.84331 8.84169 4.73435L5.38222 1.16344C5.17111 0.945519 4.82888 0.945519 4.61777 1.16344Z" fill="#737B9C" stroke="#737B9C" stroke-width="0.6"/></svg>';
        if(this.state.loading) {
            return (
                <Container>
                <Head navigation={this.navigation} route={this.route} />
                    <ScrollView style={{backgroundColor: '#1E2233'}}>
                        <Content style={{padding: 10, paddingTop: 0}}>
                            <View style={[styles.header, {justifyContent: 'space-between'}]}>
                                <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
                                    <Text style={styles.h2}>
                                        Rating Tezos bakers
                                    </Text>
                                </View>
                                <TouchableOpacity onPress={() => this.bakersUpdate()}>
                                    <SvgUri svgXmlData={svgUpdate} width="14" height="14" style={{padding: 10}} />
                                </TouchableOpacity>
                            </View>
                            <View>
                                <ScrollView>
                                    <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', marginLeft: -7, marginRight: -7, zIndex: 5000}}>
                                        <View style={styles.element}>
                                            <TextInput style={styles.search} value={searchText} placeholder="Search" onChangeText={(text) => this.inputSearch(text)} onEndEditing={(e) => this.inputSearch(e.nativeEvent.text)} placeholderTextColor="#EAECF2"/>
                                            <Image source={require('../../assets/icons/search.png')} width="16" height="16" style={{position: 'absolute', top: 14, right: 20}} />
                                        </View>
                                        <View style={[styles.element]}>
                                            <DropDownPicker
                                                items={[
                                                  { label: "Baker", value: 'rank_desc', svg: 'desc'},
                                                  { label: "Total Points", value: 'total_points', svg: 'asc'},
                                                  { label: "Total Points", value: 'total_points_desc', svg: 'desc'},
                                                  { label: "Free space", value: 'freespace', svg: 'asc'},
                                                  { label: "Free space", value: 'freespace_desc', svg: 'desc'},
                                                  { label: "Efficiency", value: 'efficiency', svg: 'asc'},
                                                  { label: "Efficiency", value: 'efficiency_all_desc', svg: 'desc'},
                                                  { label: "Last 10", value: 'efficiency_last10cycle', svg: 'asc'},
                                                  { label: "Last 10", value: 'efficiency_last10cycle_desc', svg: 'desc'},
                                                  { label: "Fee", value: 'fee', svg: 'asc'},
                                                  { label: "Fee", value: 'fee_desc', svg: 'desc'},
                                                  { label: "Yield", value: 'yield', svg: 'asc'},
                                                  { label: "Yield", value: 'yield_desc', svg: 'desc'},
                                                ]}
                                                containerStyle={{height: 45, padding: 0, }}
                                                style={{padding: 0, fontFamily: 'Circe', fontSize: 16, backgroundColor: '#090A11', borderColor: '#090A11', borderTopLeftRadius: 25, borderTopRightRadius: 25, borderBottomLeftRadius: 25, borderBottomRightRadius: 25}}
                                                dropDownStyle={{paddingVertical: -1, paddingHorizontal: 0, overflow: 'hidden', borderBottomLeftRadius: 25, borderBottomRightRadius: 25, backgroundColor: '#090A11', borderColor: "#090A11"}}
                                                itemStyle={{paddingVertical: 10, paddingHorizontal: 10, borderTopWidth: 1, borderTopColor: '#1E212B', flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center'}}
                                                arrowColor="#319DFE"
                                                activeDropItemStyle={{flexDirection: 'row', justifyContent: 'flex-start'}}
                                                activeItemStyle={{backgroundColor: '#1E2233'}}
                                                activeLabelStyle={{marginLeft: 10, fontFamily: 'Circe', fontSize: 16}}
                                                placeholderStyle={{fontFamily: 'Circe'}}
                                                labelStyle={{marginLeft: 10, color: '#fff', fontFamily: 'Circe', fontSize: 16}}
                                                onChangeItem={item => this.filter(item.value)}
                                                dropDownMaxHeight={240}
                                              />
                                        </View>
                                    </View>
                                    {this.state.baker_update ?
                                        <View style={{flex: 1, flexDirection: 'row', flexWrap: 'nowrap', borderRadius: 20, overflow: 'hidden'}}>
                                            <View style={{maxWidth: '55%', overflow: 'hidden'}}>
                                                <View style={styles.theadtr}>
                                                    <Text style={[styles.th, {width: '100%'}]}># Baker</Text>
                                                </View>
                                                {displayBakers.map((i) => (
                                                    <TouchableOpacity key={bakers[i].rank} activeOpacity={.8} onPress={() => this.navigation.navigate('Baker', {tz: bakers[i].address})}>
                                                        <View style={styles.tbodytr}>
                                                            <View style={[styles.td, {width: '100%', flex: 1, flexDirection: 'row', alignItems: 'center'}]}>
                                                                {bakers[i].pro_status ?
                                                                    <Image source={require('../../assets/icons/pro.png')} width="41" height="26" style={{marginRight: 10}}/>
                                                                    :
                                                                    <Text style={{color: '#EAECF2', fontFamily: 'Circe', fontSize: 16, marginRight: 10}}>{bakers[i].rank}</Text>
                                                                }
                                                                <View style={{width: 24, marginRight: 5}}><Image source={{uri: bakers[i].logo}} style={{resizeMode:'contain',width:null,height:null, flex:1}}/></View>
                                                                <Text style={styles.table_text}>{bakers[i].name}</Text>
                                                            </View>
                                                        </View>
                                                    </TouchableOpacity>
                                                ))}
                                            </View>
                                            <View style={{maxWidth: '45%', borderLeftWidth: 1, borderLeftColor: '#30333F'}}>
                                                <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                                                    <View>
                                                        <View style={styles.theadtr}>
                                                            <Text style={[styles.th, {width: 60}]}>Yield</Text>
                                                            <Text style={[styles.th, {width: 83}]}>Efficiency</Text>
                                                            <Text style={[styles.th, {width: 83}]}>Last 10</Text>
                                                            <Text style={[styles.th, {width: 80}]}>Fee</Text>
                                                            <Text style={[styles.th, {width: 120}]}>Free space</Text>
                                                            <Text style={[styles.th, {width: 120}]}>Total points</Text>
                                                        </View>
                                                        {displayBakers.map((i) => (
                                                                <View style={styles.tbodytr} key={bakers[i].rank}>
                                                                    <Text style={[styles.td, {width: 60}]}>{bakers[i].yield.toFixed(2)}%</Text>
                                                                    <View style={[styles.td, {width: 83}]}>
                                                                        <View style={[styles.tint, bakers[i].efficiency > 98 ? styles.green : bakers[i].efficiency > 96 ? styles.yellow : styles.red]}>
                                                                            <Text style={[styles.tint_text, {textAlign: 'center'}, , bakers[i].efficiency > 98 ? styles.green : bakers[i].efficiency > 96 ? styles.yellow : styles.red]}>{bakers[i].efficiency}%</Text>
                                                                        </View>
                                                                    </View>
                                                                    <View style={[styles.td, {width: 83}]}>
                                                                        <View style={[styles.tint, bakers[i].efficiency_last10cycle > 98 ? styles.green : bakers[i].efficiency_last10cycle > 96 ? styles.yellow : styles.red]}>
                                                                            <Text style={[styles.tint_text, {textAlign: 'center', flexWrap: 'nowrap'}, , bakers[i].efficiency_last10cycle > 98 ? styles.green : bakers[i].efficiency_last10cycle > 96 ? styles.yellow : styles.red]}> {bakers[i].efficiency_last10cycle}%</Text>
                                                                        </View>
                                                                    </View>
                                                                    <Text style={[styles.td, {width: 80}]}>{(bakers[i].fee*100).toFixed(2)}%</Text>
                                                                    <Text style={[styles.td, {width: 120}, (bakers[i].deletation_status) ? styles.green : styles.red]}>{bakers[i].freespace_min}</Text>
                                                                    <Text style={[styles.td, {width: 120, paddingLeft: 40}]}>{bakers[i].total_points}</Text>
                                                                </View>
                                                            ))}
                                                    </View>
                                                </ScrollView>
                                            </View>
                                        </View>
                                        :
                                        <Spinner color='#319DFE' />
                                    }
                                </ScrollView>
                            </View>
                        </Content>
                    </ScrollView>
                </Container>
            )
        } else {
            return (
                       <Container>
                              <Head navigation={this.navigation} route={this.route} />
                           <Content style={{backgroundColor: '#1E2233', color: '#EAECF2'}}>
                               <Spinner color='#319DFE' />
                           </Content>
                       </Container>
            )
        }
    }
}
const {width, height} = Dimensions.get('window');
if(width > 350) {
    var gee = {
        table_text: {
            color: '#EAECF2',
            fontFamily: 'Circe',
            fontSize: 16,
            flex: 1,
            flexWrap: 'wrap'
        },
        td: {
            paddingLeft: 15,
            paddingRight: 0,
            color: '#EAECF2',
            fontFamily: 'Circe',
            fontSize: 16,
            flex: 1,
            flexDirection: 'row'
        },
        tint_text: {
            fontFamily: 'Circe',
            fontSize: 14,
        },
    }
} else {
    var gee = {
        table_text: {
            color: '#EAECF2',
            fontFamily: 'Circe',
            fontSize: 13,
            flex: 1,
            flexWrap: 'wrap'
        },
        td: {
            paddingLeft: 15,
            paddingRight: 0,
            color: '#EAECF2',
            fontFamily: 'Circe',
            fontSize: 13,
            flex: 1,
            flexDirection: 'row'
        },
        tint_text: {
            fontFamily: 'Circe',
            fontSize: 12,
        },
    }
}

const styles = Object.assign(gee, {
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 30,
        marginBottom: 10
    },
    h2: {
        color: '#EAECF2',
        fontSize: 26,
        fontFamily: 'Circe_bold',
        marginRight: 11
    },
    tbodytr: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        height: 61,
        flex: 1,
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderColor: '#30333F',
        backgroundColor: '#121520'
    },
    theadtr: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        height: 61,
        flex: 1,
        justifyContent: 'center',
        flexWrap: 'nowrap'
    },
    th: {
        backgroundColor: '#0D0F17',
        paddingTop: 23,
        paddingBottom: 23,
        paddingLeft: 15,
        color: '#737B9C',
        fontFamily: 'DINNextLTPro',
        ...Platform.select({
          ios: {
            paddingTop: 24,
          },
        }),
    },
    tint: {
        fontFamily: 'Circe',
        fontSize: 16,
        paddingTop: 4,
        paddingBottom: 2,
        borderWidth: 1,
        borderRadius: 5,
        flex: 1,
    },
    tint_text: {
        fontFamily: 'Circe',
        fontSize: 14,
    },
    green: {
        color: '#30DD36',
        borderColor: '#30DD36'
    },
    red: {
        color: '#F91525',
        borderColor: '#F91525'
    },
    yellow: {
        color: '#FFAD15',
        borderColor: '#FFAD15'
    },
    element: {
        margin: 7,
        flex: 1,
        flexGrow: 1
    },
    search: {
        height: 45,
        borderRadius: 53,
        backgroundColor: '#090A11',
        paddingLeft: 20,
        paddingRight: 40,
        fontFamily: 'Circe',
        color: '#EAECF2',
        fontSize: 16,
        lineHeight: 28,
    }
});