//Homescreen.js

import React, { Component } from 'react';
import { View, SafeAreaView, StyleSheet, Image, Dimensions, ScrollView, TextInput, StatusBar, TouchableOpacity} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { Container, Header,Left, Body, Right, Title, Content, Footer, FooterTab, Button, Text, Spinner} from 'native-base';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import Head from '../../../Header';
import { ScrollView as GestureHandlerScrollView } from 'react-native-gesture-handler';
import SvgUri from 'expo-svg-uri';
import Swiper from "react-native-swiper";

export default class TezosNodesscreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
        this.navigation = props.navigation;
        this.route = props.route;
    }

  render() {
      return (
          <Container>
    <Head sub={true} navigation={this.navigation} route={this.route}/>
              <ScrollView style={{backgroundColor: '#1E2233'}}>
                  <Content style={{padding:10, paddingTop: 0}}>
                      <View style={styles.header}><Text style={styles.h2}>About</Text></View>
                      <View style={styles.block_info}>
                          <Text style={styles.text}>Tezos-Nodes is a reliability rating public Tezos bakers and a service for monitoring the state of node performance for non-public Tezos bakers.</Text>
                          <Text style={styles.text}>Working with the baking community, we've developed a baker reliability index that features 10 key indicators. We think this index will provide you with all the necessary info you need to choose a reliable public baker.</Text>
                          <Text style={styles.h3}>10 key indicators to evaluate each Tezos baker</Text>
                          <Text style={[styles.text, {marginBottom: 0}]}><Text style={styles.bold}>Lifetime</Text> — the number of node operation cycles</Text>
                          <Text style={[styles.text, {marginBottom: 0}]}><Text style={styles.bold}>Yield</Text> — yearly profit taking into account efficiency and fee</Text>
                          <Text style={[styles.text, {marginBottom: 0}]}><Text style={styles.bold}>Efficiency</Text> — the effectiveness of the node for year</Text>
                          <Text style={[styles.text, {marginBottom: 0}]}><Text style={styles.bold}>Last 10</Text> — the effectiveness of the node over the past 10 cycles</Text>
                          <Text style={[styles.text, {marginBottom: 0}]}><Text style={styles.bold}>Fee</Text> — charges to run their node service</Text>
                          <Text style={[styles.text, {marginBottom: 0}]}><Text style={styles.bold}>Support</Text> — availability of support 24/7 for delegates</Text>
                          <Text style={[styles.text, {marginBottom: 0}]}><Text style={styles.bold}>Projects</Text> — Baker's personal projects. Payouts — payout period</Text>
                          <Text style={[styles.text, {marginBottom: 0}]}><Text style={styles.bold}>Free Space</Text> — free space for delegation</Text>
                          <Text style={[styles.text]}><Text style={styles.bold}>Total Points</Text> — points reliability</Text>
                          <Text style={styles.h3}>Data source</Text>
                          <Text style={styles.text}>We receive data for reliability indicators Lifetime, Efficiency*, Last 10, Free Space, automatically from the Tezos blockchain using RPC commands from our own Tezos node and from the public API of the TzStats interface. For data on Fees, Support, Projects and Payouts, we constantly monitor baker behaviors, and track this information on an ongoing basis. Our team clarifies details with individual bakers, and we have the last word on points assigned to subjective categories like "Projects". The Total Points are summed up automatically according to the given values for each of the reliability criteria.</Text>
                          <Text style={styles.small}>*Efficiency is calculated taking into baking right and steal</Text>
                          <Text style={styles.h3}>Reliability evaluate system</Text>
                          <Text style={styles.text}>In order to make your research as simple as possible, we have developed a 100-point system for determining the reliability of bakers, namely:</Text>
                          <View style={{height: 380, marginHorizontal: -10, flex: 1}}>
                              <Swiper
                                  style={[styles.wrapper]}
                                  onMomentumScrollEnd={(e, state, context) =>
                                    console.log('index:', state.index)
                                  }
                                  dot={
                                    <View
                                      style={{
                                        backgroundColor: '#1E2233',
                                        width: 9,
                                        height: 9,
                                        borderRadius: 5,
                                        marginLeft: 4,
                                        marginRight: 4,
                                        marginTop: 3,
                                        marginBottom: 3
                                      }}
                                    />
                                  }
                                  activeDot={
                                    <View
                                      style={{
                                        backgroundColor: '#319DFE',
                                        width: 9,
                                        height: 9,
                                        borderRadius: 5,
                                        marginLeft: 4,
                                        marginRight: 4,
                                        marginTop: 3,
                                        marginBottom: 3
                                      }}
                                    />
                                  }
                                  paginationStyle={{
                                    position: 'absolute',
                                    bottom: -20,
                                    left: 10,
                                    right: 10,
                                    paddingBottom: 20
                                  }}
                                  loop
                                >
                                  <View style={styles.slide}>
                                    <Text style={styles.h3_slider}>Efficiency for last year:</Text>
                                    <Text style={styles.bold_slider}>Max. 30 points</Text>
                                    <Text style={styles.text_slider}>30 points for 100% +</Text>
                                    <Text style={styles.text_slider}>25 points for 99,5-99,99%</Text>
                                    <Text style={styles.text_slider}>20 points for 99,01-99,49%</Text>
                                    <Text style={styles.text_slider}>15 points for 98,01-99%</Text>
                                    <Text style={styles.text_slider}>10 points for 97,01-98%</Text>
                                    <Text style={styles.text_slider}>5 points for 96,01-97%</Text>
                                    <Text style={styles.text_slider}>0 points for 0-96%</Text>
                                  </View>
                                  <View style={styles.slide}>
                                    <Text style={styles.h3_slider}>Efficiency over the last 10 cycles:</Text>
                                    <Text style={styles.bold_slider}>Max. 20 points</Text>
                                    <Text style={styles.text_slider}>20 points for 100% +</Text>
                                    <Text style={styles.text_slider}>15 points for 99,5-99,99%</Text>
                                    <Text style={styles.text_slider}>12 points for 99,01-99,49%</Text>
                                    <Text style={styles.text_slider}>10 points for 98,01-99%</Text>
                                    <Text style={styles.text_slider}>7 points for 97,01-98%</Text>
                                    <Text style={styles.text_slider}>5 points for 96,01-97%</Text>
                                    <Text style={styles.text_slider}>0 points for 0-96%</Text>
                                  </View>
                                  <View style={styles.slide}>
                                    <Text style={styles.h3_slider}>Fee:</Text>
                                    <Text style={styles.bold_slider}>Max. 20 points</Text>
                                    <Text style={styles.text_slider}>20 points for 0 - 4,99%</Text>
                                    <Text style={styles.text_slider}>17 points for 5 – 7,99%</Text>
                                    <Text style={styles.text_slider}>15 points for 8 - 9,99%</Text>
                                    <Text style={styles.text_slider}>12 points for 10 - 12,49%</Text>
                                    <Text style={styles.text_slider}>10 points for 12,5 – 14,99%</Text>
                                    <Text style={styles.text_slider}>5 points for 15 – 19,99%</Text>
                                    <Text style={styles.text_slider}>0 points for 20 - 100%</Text>
                                  </View>
                                  <View style={styles.slide}>
                                    <Text style={styles.h3_slider}>Support:</Text>
                                    <Text style={styles.bold_slider}>Max. 10 points</Text>
                                    <Text style={styles.text_slider}>10 points - There is 24/7 support chat</Text>
                                    <Text style={styles.text_slider}>5 points – Representative in social and media spaces</Text>
                                    <Text style={styles.text_slider}>0 points – Only website</Text>
                                  </View>
                                  <View style={styles.slide}>
                                    <Text style={styles.h3_slider}>Projects:</Text>
                                    <Text style={styles.bold_slider}>Max. 10 points</Text>
                                    <Text style={styles.text_slider}>10 points - Makes projects (tools) to improve the ecosystem Tezos</Text>
                                    <Text style={styles.text_slider}>5 points – Organization of meetings</Text>
                                    <Text style={styles.text_slider}>0 points – Only bakes</Text>
                                  </View>
                                  <View style={styles.slide}>
                                    <Text style={styles.h3_slider}>Payouts:</Text>
                                    <Text style={styles.bold_slider}>Max. 10 points</Text>
                                    <Text style={styles.text_slider}>10 points – Pays rewards without freezing (after 7 cycles or earlier) </Text>
                                    <Text style={styles.text_slider}>5 points – Pays rewards immediately after defrosting (after 12 cycles) </Text>
                                    <Text style={styles.text_slider}>0 points – Pays rewards not in the first cycle after defrosting (undefined)</Text>
                                  </View>
                                </Swiper>
                          </View>
                          <Text style={styles.h3}>Monitoring the state of node performance non-public Tezos bakers</Text>
                          <Text style={[styles.text, {marginBottom: 0}]}>On Tezos-Nodes we have provided an opportunity for non-public Tezos bakers to monitor effectiveness their node in the same convenient mode as for public Tezos bakers. After registration personal account you can check the effectiveness and free space for new delegations of absolutely any active Tezos baker.</Text>
                      </View>
                  </Content>
              </ScrollView>
          </Container>
      );
  }
}

const styles = {
    active_menu: {
        minHeight: '100%',
        maxHeight: '100%'
    },
    text: {
        fontSize: 22,
        fontFamily: 'Circe',
        color: '#EAECF2'
    },
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 30,
        marginBottom: 10
    },
    h2: {
        color: '#EAECF2',
        fontSize: 26,
        fontFamily: 'Circe_bold',
        marginRight: 11
    },
    block_info: {
        backgroundColor: "#121520",
        borderRadius: 20,
        padding: 20
    },
    text: {
        color: '#737B9C',
        fontSize: 18,
        fontFamily: 'Circe',
        marginBottom: 18,
        lineHeight: 22
    },
    text_slider: {
        color: '#737B9C',
        fontSize: 18,
        fontFamily: 'Circe',
        marginBottom: 4,
        lineHeight: 24
    },
    bold: {
        fontFamily: 'Circe_bold',
        fontSize: 18,
        color: '#319DFE'
    },
    bold_slider: {
        fontFamily: 'Circe_bold',
        fontSize: 20,
        color: '#319DFE',
        marginBottom: 20,
    },
    small: {
        fontFamily: 'Circe',
        fontSize: 11,
        color: '#737B9C',
        marginBottom: 18,
    },
    h3: {
        color: '#EAECF2',
        fontSize: 22,
        lineHeight: 26,
        fontFamily: 'Circe_bold',
        marginVertical: 10
    },
    h3_slider: {
        color: '#EAECF2',
        fontSize: 22,
        lineHeight: 26,
        fontFamily: 'Circe_bold'
    },
    container: {
        flex: 1
    },
    slide: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#1E2233',
        padding: 25,
        borderRadius: 20,
        marginBottom: 30,
        marginHorizontal: 10
    },
}