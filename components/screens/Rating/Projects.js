import React, { Component } from 'react';
import { View, SafeAreaView, StyleSheet, Image, Dimensions, ScrollView, TextInput, StatusBar, TouchableOpacity, Text, Linking} from 'react-native';
import { ScrollView as GestureHandlerScrollView } from 'react-native-gesture-handler';
import SvgUri from 'expo-svg-uri';

export default class Projects extends Component {
    constructor(props) {
        super(props);
        this.baker = props.baker;
    }
    render() {
        return (
            <View>
                <View style={styles.header}>
                    <Text style={styles.h2}>
                        Personal Baker Projects
                    </Text>
                </View>
                <View style={[styles.block_info, {maxHeight: 252}]}>
                    <GestureHandlerScrollView>
                        {this.baker.projects.map((project, i) => (
                            <TouchableOpacity key={i} onPress={() => Linking.openURL(project.url)}>
                                <View style={styles.row}>
                                    <SvgUri source={{uri: project.icon}} width="23" height="23" style={{marginLeft: 25, marginRight: 25}} />
                                    <Text style={{fontFamily: 'Circe', fontSize: 16, color: '#EAECF2', paddingRight: 25, flex: 1, flexWrap: 'wrap'}}>{project.title}</Text>
                                </View>
                            </TouchableOpacity>
                        ))}
                    </GestureHandlerScrollView>
                </View>
            </View>
        )
    }
}
const styles = {
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 30,
        marginBottom: 10
    },
    h2: {
        color: '#EAECF2',
        fontSize: 26,
        fontFamily: 'Circe_bold',
        marginRight: 11
    },
    block_info: {
        borderRadius: 20,
        overflow: 'hidden',
        backgroundColor: "#121520"
    },
    row: {
        height: 84,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderBottomColor: '#1E212B'
    }
}