Tezos Nodes is a reliability rating public Tezos Bakers and a service for monitoring the state of node performance for non-public Tezos Bakers.

Working with the baking community, we've developed a baker reliability index that features 10 key indicators. We think this index will provide you with all the necessary info you need to choose a reliable public Baker.

The Tezos Nodes app has many features and important data provide such as:

• Reliability rating Tezos Bakers
• Tezos network data
• News about Tezos project  
• The Rewards calculator
• In personal account Bakers will be able to manage important data. All users will be able to get information about the efficiency and free space for delegation about any node in the Tezos network.
• The app also contains a list of inactive Bakers who have closed their bakery service. If you still delegate any of them, please immediately change the Baker.

Support and feedback:

If you have any issues using our application or would like to send feedback, send us an email at support@tezos-nodes.com or contact us using the application, namely support tab in the menu.

This application was created with the support of Tezos Foundation https://tezos.foundation

Designed by Expo Platform https://expo.io

#BUILD & RUN IT

Clone the `Tezos Nodes mobile App` repository and `cd` into the cloned folder.

In your console:

#Cloning App
`git clone https://gitlab.com/tezosnodes/tezos-nodes-mobile-app` 
`cd tezos-nodes-mobile-app`

#Install App
`npm i`
`expo start`

#Build App
`expo build:ios`
`expo build:android`

Before building, you need to edit App.json, instructions for editing `App.json` here `https://docs.expo.io/workflow/configuration/`









