//Homescreen.js

import React, { Component } from 'react';
import { View, SafeAreaView, StyleSheet, Image, Dimensions, ScrollView, TextInput, StatusBar, TouchableOpacity, Linking, AsyncStorage} from 'react-native';
import { Container, Header,Left, Body, Right, Title, Content, Footer, FooterTab, Button, Text, Toast, Spinner} from 'native-base';
import SvgUri from 'expo-svg-uri';
import Head from '../../Header';

import SwitchToggle from "react-native-switch-toggle";

export default class Cabinetscreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            baker: {},
            token: props.route.params.token,
            token_type: props.route.params.token_type,
            min_delegations_amount: 0,
            fee: 0,
            accepts_new_delegations: false
        }
        this.navigation = props.navigation;
        this.route = props.route;
    }

    componentDidMount() {
        fetch('https://api.tezos-nodes.com/api/auth/baker', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept': 'application/json',
                'Authorization': this.state.token_type + " " + this.state.token
            }
        })
        .then(response => response.json())
        .then(json => {
            this.setState({baker: json})
            this.setState({min_delegations_amount: json.minimal_delegation})
            this.setState({fee: json.fee})
            this.setState({accepts_new_delegations: !json.accepts_new_delegations})
            this.setState({loading: true})
        })
    }

    onChangedAmount(text){
        let newText = '';
        let numbers = '0123456789';

        for (var i=0; i < text.length; i++) {
            if(numbers.indexOf(text[i]) > -1 ) {
                newText = newText + text[i];
            }
            else {
                // your call back function
                alert("please enter numbers only");
            }
        }
        this.setState({ min_delegations_amount: newText });
    }

    onChangedFee(text){
        let newText = '';
        let numbers = '0123456789,.';

        for (var i=0; i < text.length; i++) {
            if(numbers.indexOf(text[i]) > -1 ) {
                newText = newText + text[i];
            }
            else {
                // your call back function
                alert("please enter numbers only");
            }
        }
        this.setState({ fee: newText });
    }

    authorized() {
        AsyncStorage.getItem('token')
        .then((token) => {
            if(token !== null) {
                this.setState({token: token})
            }
        })
        AsyncStorage.getItem('token_type')
        .then((token_type) => {
            if(token_type !== null) {
                this.setState({token_type: token_type})
            }
        })
    }

    saveForm() {
        fetch('https://api.tezos-nodes.com/api/auth/baker_update',{
            method: "POST",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept': 'application/json',
                'Authorization': this.state.token_type + " " + this.state.token
            },
            body: 'fee=' + this.state.fee + '&minimal_delegation=' + this.state.min_delegations_amount + "&accepts_new_delegations=" + this.state.accepts_new_delegations
        })
        .then(response => response.json())
        .then(json => {
            Toast.show({
              text: 'The update was successful!',
              textStyle: {textAlign: 'center'},
              type: 'success',
              duration: 2000
            })
            console.log(json);
        })
    }

  render() {
    if(this.state.loading) {
        let {baker} = this.state;
        return (
            <Container>
        <Head sub={true} navigation={this.navigation} route={this.route} />
                <ScrollView style={{backgroundColor: '#1E2233'}}>
                    <Content style={{padding: 10, paddingTop: 0}}>
                        <View style={styles.info_block}>
                            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', marginVertical: 20}}>
                                <View style={{flex: 1, width: 85, height: 85}}><Image source={{uri: 'https://tezos-nodes.com/storage/' + baker.photo.url}} style={{resizeMode:'contain',width:null,height:null, flex:1}} /></View>
                            </View>
                            <Text style={{fontFamily: "DINNextLTPro_bold", fontSize: 20,color: '#EAECF2', textAlign: 'center'}}>{baker.title}</Text>
                            <View style={{marginTop: 30, marginBottom: 20}}>
                                <Text style={styles.title}>Min delegations amount</Text>
                                <TextInput style={styles.input} value={`${this.state.min_delegations_amount}`} keyboardType='numeric' onChangeText={(text) => this.onChangedAmount(text)} onEndEditing={(e) => e.nativeEvent.text == '' ? this.setState({min_delegations_amount: 0}) : ''}></TextInput>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', marginLeft: -7.5, marginRight: -7.5}}>
                                <View style={{flex: 1, flexGrow: 1, marginHorizontal: 7.5}}>
                                    <Text style={styles.title}>Fee:</Text>
                                    <TextInput style={styles.input} value={`${this.state.fee}`} keyboardType='numeric' onChangeText={(text) => this.onChangedFee(text)} onEndEditing={(e) => e.nativeEvent.text == '' ? this.setState({fee: 0}) : ''}></TextInput>
                                </View>
                                <View style={{flex: 1, flexGrow: 1, marginHorizontal: 7.5}}>
                                    <Text style={styles.title}>New delegations:</Text>
                                    <View style={{flex: 1, alignItems: 'center'}}>
                                        <SwitchToggle
                                          containerStyle={{
                                            marginTop: 8,
                                            width: 98,
                                            height: 45,
                                            borderRadius: 25,
                                            backgroundColor: "#090A11",
                                            padding: 5
                                          }}
                                          backgroundColorOn="#090A11"
                                          backgroundColorOff="#090A11"
                                          circleStyle={{
                                            width: 38,
                                            height: 38,
                                            borderRadius: 19,
                                            backgroundColor: "#090A11" // rgb(102,134,205)
                                          }}
                                          switchOn={this.state.accepts_new_delegations}
                                          onPress={() => this.setState({accepts_new_delegations: !this.state.accepts_new_delegations})}
                                          circleColorOff="#F91525"
                                          circleColorOn="#30DD36"
                                          duration={300}
                                        />
                                    </View>
                                </View>
                            </View>
                            <Button style={styles.save} onPress={() => this.saveForm()}><Text style={styles.save_text}>Save</Text></Button>
                        </View>
                    </Content>
                </ScrollView>
            </Container>
        )
    } else {
        return (
           <Container>
      <Head sub={true} navigation={this.navigation} route={this.route} />
               <Content style={{backgroundColor: '#1E2233', color: '#EAECF2'}}>
                   <Spinner color='#319DFE' />
               </Content>
           </Container>
        )
    }
  }
}

const styles = StyleSheet.create({
    info_block: {
        padding: 16,
        marginTop: 30,
        borderRadius: 20,
        backgroundColor: '#121520'
    },
    title: {
        fontFamily: 'DINNextLTPro',
        fontSize: 18,
        color: '#737B9C',
        opacity: 0.6
    },
    input: {
        height: 60,
        paddingLeft: 26,
        backgroundColor: '#090A11',
        borderRadius: 12,
        color: '#737B9C',
        fontFamily: 'Circe'
    },
    save: {
        borderRadius: 12,
        justifyContent: 'center',
        backgroundColor: '#319DFE',
        height: 60,
        marginTop: 20
    },
    save_text: {

    }
});