import React, { Component } from 'react';
import { View, SafeAreaView, StyleSheet, Image, Dimensions, ScrollView, TextInput, StatusBar, TouchableOpacity, Text, Linking} from 'react-native';
import SvgUri from 'expo-svg-uri';

export default class Contacts extends Component {
    constructor(props) {
        super(props);
        this.baker = props.baker;
    }
    render() {
        const baker = this.baker;
        return (
            <View>
                <View style={styles.header}>
                    <Text style={styles.h2}>
                        Baker contact
                    </Text>
                </View>
                <View style={styles.info_block}>
                    {Object.keys(baker.contacts).map((item, i) => (
                        <TouchableOpacity style={styles.link} key={i} onPress={() => {Linking.openURL(baker.contacts[item].url)}}>
                            <SvgUri source={{uri: baker.contacts[item].icon}} width="22" height="22" style={{marginRight: 20}} />
                            <Text style={styles.link_text}>{baker.contacts[item].title}</Text>
                        </TouchableOpacity>
                    ))}
                </View>
            </View>
        );
    }
}
const styles = {
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 30,
        marginBottom: 10
    },
    h2: {
        color: '#EAECF2',
        fontSize: 26,
        fontFamily: 'Circe_bold',
        marginRight: 11
    },
    info_block: {
        padding: 15,
        backgroundColor: '#121520',
        borderRadius: 20,
        marginBottom: 29,
        paddingBottom: 5
    },
    link: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 21,
        backgroundColor: '#1E2233',
        height: 54,
        borderRadius: 12,
        marginBottom: 10,
    },
    link_text: {
        fontFamily: 'Circe',
        color: '#EAECF2',
        fontSize: 16,
        lineHeight: 55
    }
};