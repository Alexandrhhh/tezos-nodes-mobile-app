import React, { Component } from 'react';
import { View, SafeAreaView, StyleSheet, Image, Dimensions, ScrollView, TextInput, StatusBar, TouchableOpacity, Text} from 'react-native';
import { ScrollView as GestureHandlerScrollView } from 'react-native-gesture-handler';
import SvgUri from 'expo-svg-uri';

export default class Vote extends Component {

    constructor(props) {
        super(props);
        this.state = {
            baker: props.baker
        }
    }

    render() {
        const svgYay = '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M11.2121 16.3768C11.1071 16.4824 10.9638 16.5413 10.815 16.5413C10.6662 16.5413 10.523 16.4824 10.418 16.3768L7.24682 13.2051C6.91773 12.876 6.91773 12.3424 7.24682 12.0139L7.64389 11.6167C7.97309 11.2877 8.50612 11.2877 8.83522 11.6167L10.815 13.5967L16.1648 8.24682C16.494 7.91773 17.0275 7.91773 17.3561 8.24682L17.7532 8.644C18.0823 8.97309 18.0823 9.50664 17.7532 9.83522L11.2121 16.3768Z" fill="#30DD36"/></svg>';
        let {baker} = this.state;
        if(baker.voting != null) {
            var voting = Object.keys(baker.voting);
            voting = voting.sort((a, b) => {
                return b-a;
            });

        }
        return (
            <View>
                <View style={styles.header}>
                    <Text style={styles.h2}>
                        Voting Overview
                    </Text>
                </View>
                <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                <View style={{borderRadius: 20, overflow: 'hidden'}}>
                    <View style={styles.theadtr}>
                        <Text style={[styles.th, {flexGrow: 1, minWidth: 90}]}>Proposale name</Text>
                        <Text style={[styles.th, {width: 83, minWidth: 83, maxWidth: 83, textAlign: 'center'}]}>Proposale vote</Text>
                        <Text style={[styles.th, {width: 70, minWidth: 70, maxWidth: 70, textAlign: 'center'}]}>Testing vote</Text>
                        <Text style={[styles.th, {width: 85, minWidth: 85, maxWidth: 85, textAlign: 'center'}]}>Promotion vote</Text>
                    </View>
                    <View style={{maxHeight: 200, backgroundColor: '#121520'}}>
                        <GestureHandlerScrollView>
                            {baker.voting !== null ? voting.map((item, i) => (
                                <View key={i} style={styles.tbodytr}>
                                    <Text style={[styles.td, {flexGrow: 1, minWidth: 90, paddingLeft: 15}]}>{baker.voting[item].proposal_name}</Text>
                                    <View style={[styles.td, {width: 83, minWidth: 83, maxWidth: 83, justifyContent: 'center'}]}>{baker.voting[item].proposal_vote == 'yay' ? <SvgUri svgXmlData={svgYay} width="24" height="24" /> : <Text></Text>}</View>
                                    <Text style={[styles.td, {width: 70, minWidth: 70, maxWidth: 70, textAlign: 'center'}]}>{baker.voting[item].testing_vote}</Text>
                                    <Text style={[styles.td, {width: 85, minWidth: 85, maxWidth: 85, textAlign: 'center'}]}>{baker.voting[item].promotion_vote}</Text>
                                </View>
                            )) : <Text></Text>}
                        </GestureHandlerScrollView>
                    </View>
                </View>
                </ScrollView>
            </View>
        )
    }
}


const {width, height} = Dimensions.get('window');
if(width > 350) {
    var gee = {
        h2: {
            color: '#EAECF2',
            fontSize: 26,
            fontFamily: 'Circe_bold',
            marginRight: 11
        },
    }
} else {
    var gee = {
        h2: {
            color: '#EAECF2',
            fontSize: 20,
            fontFamily: 'Circe_bold',
            marginRight: 11
        },
    }
}
const styles = Object.assign(gee, {
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 30,
        marginBottom: 10
    },
    h2: {
        color: '#EAECF2',
        fontSize: 26,
        fontFamily: 'Circe_bold',
        marginRight: 11
    },
    tbodytr: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        height: 61,
        flex: 1,
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderColor: '#30333F',
        backgroundColor: '#121520'
    },
    theadtr: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        height: 'auto',
        justifyContent: 'center',
        flexWrap: 'nowrap',
        backgroundColor: '#0D0F17',
        paddingLeft: 6,
        paddingRight: 6
    },
    th: {
        flex: 1,
        flexWrap: 'wrap',
        backgroundColor: '#0D0F17',
        paddingTop: 23,
        paddingBottom: 23,
        paddingLeft: 6,
        paddingRight: 6,
        fontSize: 16,
        color: '#737B9C',
        justifyContent: 'center',
        fontFamily: 'DINNextLTPro',

    },
    td: {
        paddingLeft: 0,
        paddingRight: 6,
        color: '#EAECF2',
        fontFamily: 'Circe',
        fontSize: 16,
        flex: 1,
        flexDirection: 'row'
    },
    table_text: {
        color: '#EAECF2',
        fontFamily: 'Circe',
        fontSize: 16,
        flex: 1,
        flexWrap: 'wrap',
    }
})